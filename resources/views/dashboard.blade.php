@extends('layouts.mainlayout')

@section('title', 'Dashboard')
    
@section('content')

    <h1>Welcome, {{Auth::user()->username}} <i class="bi bi-box-seam"></i>
    </h1> <i class="bi bi-0-circle"></i>
    
    <div class="row mt-5">
        <div class="col-lg-4">
            <div class="card-data cargo"> 
                <div class="row">
                    <div class="col-6">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-box-seam card-data-svg" viewBox="0 0 16 16">
                            <path d="M8.186 1.113a.5.5 0 0 0-.372 0L1.846 3.5l2.404.961L10.404 2zm3.564 1.426L5.596 5 8 5.961 14.154 3.5zm3.25 1.7-6.5 2.6v7.922l6.5-2.6V4.24zM7.5 14.762V6.838L1 4.239v7.923zM7.443.184a1.5 1.5 0 0 1 1.114 0l7.129 2.852A.5.5 0 0 1 16 3.5v8.662a1 1 0 0 1-.629.928l-7.185 2.874a.5.5 0 0 1-.372 0L.63 13.09a1 1 0 0 1-.63-.928V3.5a.5.5 0 0 1 .314-.464z"/>
                        </svg>
                    </div>
                    <div class="col-6 d-flex flex-column justify-content-center">
                        <div class="card-desc">Cargo</div>
                        <div class="card-count">{{$cargo_count}}</div>                        
                    </div>
                </div>                
            </div>
        </div> 
        <div class="col-lg-4">
            <div class="card-data categories"> 
                <div class="row">
                    <div class="col-6">
                        <svg xmlns="http://www.w3.org/2000/svg" width="80" height="80" fill="currentColor" class="bi bi-list-task" viewBox="0 0 16 16">
                            <path fill-rule="evenodd" d="M2 2.5a.5.5 0 0 0-.5.5v1a.5.5 0 0 0 .5.5h1a.5.5 0 0 0 .5-.5V3a.5.5 0 0 0-.5-.5zM3 3H2v1h1z"/>
                            <path d="M5 3.5a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5M5.5 7a.5.5 0 0 0 0 1h9a.5.5 0 0 0 0-1zm0 4a.5.5 0 0 0 0 1h9a.5.5 0 0 0 0-1z"/>
                            <path fill-rule="evenodd" d="M1.5 7a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5H2a.5.5 0 0 1-.5-.5zM2 7h1v1H2zm0 3.5a.5.5 0 0 0-.5.5v1a.5.5 0 0 0 .5.5h1a.5.5 0 0 0 .5-.5v-1a.5.5 0 0 0-.5-.5zm1 .5H2v1h1z"/>
                          </svg>
                    </div>
                    <div class="col-6 d-flex flex-column justify-content-center">
                        <div class="card-desc">Categories</div>
                        <div class="card-count">{{$category_count}}</div>                        
                    </div>
                </div>                
            </div>
        </div> 
        <div class="col-lg-4">
            <div class="card-data user"> 
                <div class="row">
                    <div class="col-6">
                        <svg xmlns="http://www.w3.org/2000/svg" width="80" height="80" fill="currentColor" class="bi bi-people-fill" viewBox="0 0 16 16">
                            <path d="M7 14s-1 0-1-1 1-4 5-4 5 3 5 4-1 1-1 1zm4-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6m-5.784 6A2.24 2.24 0 0 1 5 13c0-1.355.68-2.75 1.936-3.72A6.3 6.3 0 0 0 5 9c-4 0-5 3-5 4s1 1 1 1zM4.5 8a2.5 2.5 0 1 0 0-5 2.5 2.5 0 0 0 0 5"/>
                          </svg>
                    </div>
                    <div class="col-6 d-flex flex-column justify-content-center">
                        <div class="card-desc">User</div>
                        <div class="card-count">{{$user_count}}</div>                        
                    </div>
                </div>                
            </div>
        </div>           
    </div>
    <div class="mt-5">
        <h2>#Rent Log</h2>
        <table class="table">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>User</th>
                    <th>Pet Cargo</th>
                    <th>Rent Date</th>
                    <th>Return Date</th>
                    <th>Actual Return</th>
                    <th>Status</th>                    
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td colspan="7" style="text-align: center"> 
                    No Data 
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
@endsection