<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Pet Cargo Rent | @yield('title')</title>    
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
</head>
<body>


    <div class="main d-flex flex-column justify-content-between">
        <nav class="navbar navbar-dark navbar-expand-lg bg-primary">
            <div class="container-fluid">
            <a class="navbar-brand" href="#">Pet Cargo Rent</a> 
              <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navtog" aria-controls="navtog" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>                           
            </div>
          </nav>
        <div class="body-content h-100">
            <div class="row g-0 h-100">
                <div class="sidebar col-lg-2 collapse d-lg-block" id="navtog">   
                    
                        @if(Auth::user()->role_id == 1)
                        
                        <a href="dashboard" @if (request()->route()->uri == 'dashboard') class='active'                            
                        @endif>Dashboard</a>
                        <a href="cargo" @if (request()->route()->uri == 'cargo') class='active'                            
                            @endif> Cargo</a>
                        <a href="categories" @if (request()->route()->uri == 'categories') class='active'                            
                            @endif>Category</a>                        
                        <a href="users" @if (request()->route()->uri == 'users') class='active'                            
                            @endif>Users</a>
                        <a href="rent-logs" @if (request()->route()->uri == 'rent-logs') class='active'                            
                            @endif>Rent Log</a>
                        <a href="logout">Logout</a> 
                                             
                        @else
                        <a href="profile" @if (request()->route()->uri == 'profile') class='active'                            
                            @endif>Profile</a>
                        <a href="logout">Logout</a>       
                        @endif
                                   
                </div>
                <div class="content p-5 col-lg-10">
                    @yield('content')
                </div>
            </div>
        </div>
    </div>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>   
</body>
</html>