<?php

namespace App\Http\Controllers;

use App\Models\Cargo; // Corrected model name
use App\Models\Category;
use App\Models\User;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        $cargocount = Cargo::count();
        $categorycount = Category::count();
        $userCount = User::count();        
        return view('dashboard', ['cargo_count'=>$cargocount, 'category_count' => $categorycount,'user_count'=> $userCount]);
    }
}
